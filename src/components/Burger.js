import React, { Component } from "react";
import Button from "./UI/Button";
import "./Burger.css";

export default class Burger extends Component {
  state = {
    AlooTikki: 0,
    Paneer: 0,
    Cheese: 0,
  };

  addRemoveIngredient = (action, ingredient) => {
    let { AlooTikki, Paneer, Cheese } = this.state;

    let stateValue;
    switch (ingredient) {
      case "AlooTikki": {
        stateValue = AlooTikki;
        break;
      }
      case "Paneer": {
        stateValue = Paneer;
        break;
      }
      case "Cheese": {
        stateValue = Cheese;
        break;
      }
      default:
        break;
    }
    if (action === "add") {
      stateValue = stateValue + 1;
    } else {
      stateValue = stateValue - 1;
    }
    this.setState({
      [ingredient]: stateValue >= 0 ? stateValue : 0,
    });
  };

  burgerContent = () => {
    let { AlooTikki, Paneer, Cheese } = this.state;
    let burger = [];

    for (let i = 0; i < AlooTikki; i++) {
      burger.push(<div key={burger.length} className="AlooTikkiSide"></div>);
    }
    for (let i = 0; i < Paneer; i++) {
      burger.push(<div key={burger.length} className="PaneerSide"></div>);
    }
    for (let i = 0; i < Cheese; i++) {
      burger.push(<div key={burger.length} className="CheeseSide"></div>);
    }
    if (burger.length === 0) burger.push(<p key="0">Add Slices!!</p>);
    return burger;
  };

  render() {
    return (
      <>
        <div className="burgerIngredients">
          <div className="topSide"></div>
          {this.burgerContent()}
          <div className="bottomSide"></div>
        </div>
        <div className="ingredientsBlock">
          <p>AlooTikki</p>
          <div className="ingrBtns">
            <Button
              onClick={() => this.addRemoveIngredient("add", "AlooTikki")}
            >
              Add
            </Button>
            <Button
              onClick={() => this.addRemoveIngredient("remove", "AlooTikki")}
            >
              Remove
            </Button>
          </div>
          <p>Paneer</p>
          <div className="ingrBtns">
            <Button onClick={() => this.addRemoveIngredient("add", "Paneer")}>
              Add
            </Button>
            <Button
              onClick={() => this.addRemoveIngredient("remove", "Paneer")}
            >
              Remove
            </Button>
          </div>
          <p>Cheese</p>
          <div className="ingrBtns">
            <Button onClick={() => this.addRemoveIngredient("add", "Cheese")}>
              Add
            </Button>
            <Button
              onClick={() => this.addRemoveIngredient("remove", "Cheese")}
            >
              Remove
            </Button>
          </div>
        </div>
      </>
    );
  }
}
